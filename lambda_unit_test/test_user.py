from pathlib import Path
import sys
path = str(Path(Path(__file__).parent.absolute()).parent.absolute())
sys.path.insert(0, path)

from lambda_source_code.User import User
import boto3
import pytest
from moto import mock_dynamodb

table_name = 'tf-users'
region = 'ap-south-1'
item = {
        'UserId': "1",
        'UserName': "John Doe"
       }

@pytest.fixture
def use_moto():
    @mock_dynamodb
    def dynamodb_client():
        dynamodb = boto3.resource('dynamodb', region_name=region)

        # Create the table
        dynamodb.create_table(
            TableName=table_name,
            KeySchema=[
                {
                    'AttributeName': 'UserId',
                    'KeyType': 'HASH'
                }
            ],
            AttributeDefinitions=[
                {
                    'AttributeName': 'UserId',
                    'AttributeType': 'S'
                }
            ],
            BillingMode='PAY_PER_REQUEST'
        )
        return dynamodb
    return dynamodb_client

@mock_dynamodb
def test_getUserById(use_moto):
    use_moto()
    table = boto3.resource('dynamodb', region_name=region).Table(table_name)
    table.put_item(Item = item)
    User.table = table;
    return_data = User.getUserById('1')
    print(return_data)
    assert return_data is not None

@mock_dynamodb
def test_getUserById_fail(use_moto):
    use_moto()
    table = boto3.resource('dynamodb', region_name=region).Table(table_name)
    User.table = table
    return_data = User.getUserById('1')
    print(return_data)
    assert return_data is None

@mock_dynamodb
def test_update(use_moto):
    use_moto()
    table = boto3.resource('dynamodb', region_name=region).Table(table_name)
    table.put_item(Item = item)
    User.table = table
    user = User(**item)
    status = user.update('Poulami')
    print(status)
    assert status == 200

@mock_dynamodb
def test_delete(use_moto):
    use_moto()
    table = boto3.resource('dynamodb', region_name=region).Table(table_name)
    table.put_item(Item = item)
    User.table = table
    user = User(**item)
    status = user.delete()
    print(status)
    assert status == 200
