from pathlib import Path
import sys
path = str(Path(Path(__file__).parent.absolute()).parent.absolute())
sys.path.insert(0, path)

from lambda_source_code.S3Bucket import S3Bucket
import boto3
import pytest
from moto import mock_s3

BUCKET_NAME = "test_bucket"

item = {
        'UserId': "1",
        'UserName': "John Doe"
       }

@pytest.fixture
def use_moto():
    @mock_s3
    def s3_client():
        s3 = boto3.client(
                "s3",
                region_name="us-east-1",
                aws_access_key_id="fake_access_key",
                aws_secret_access_key="fake_secret_key",
            )
        s3.create_bucket(Bucket=BUCKET_NAME)
        return s3
    return s3_client

@mock_s3
def test_saveObj(use_moto):
    use_moto()
    S3Bucket.client = boto3.client('s3')
    S3Bucket.bucketName = BUCKET_NAME
    response = None
    try:
        response = S3Bucket.saveObj("test", "userdata.json")
    except Exception as e:
        response = e
    assert response is None

def test_saveObj_fail():
    S3Bucket.bucketName = BUCKET_NAME
    response = None
    try:
        response = S3Bucket.saveObj("test", "userdata.json")
    except Exception as e:
        response = e
    assert response is not None