from lambda_source_code import lambda_handler
from lambda_source_code.User import User

item = {
        'UserId': "1",
        'UserName': "John Doe"
       }
user = User(**item)

def test_lambda_handler_get(mocker):
    event = {
        "httpMethod": "GET",
        "queryStringParameters": {"id":"1"}
    }
    mocker.patch("User.User.getUserById", return_value=user)
    mocker.patch("S3Bucket.S3Bucket.saveObj", return_value=None) 
    response = lambda_handler.handler(event, None)
    body = response['body']
    assert body == "User with id#1 found and saved to S3 bucket" 

def test_lambda_handler_get_not_found(mocker):
    event = {
        "httpMethod": "GET",
        "queryStringParameters": {"id":"1"}
    }
    mocker.patch("User.User.getUserById", return_value=None)
    response = lambda_handler.handler(event, None)
    body = response['body']
    assert body == "User with id#1 not found"

def test_lambda_handler_missing_param(mocker):
    event = {
        "httpMethod": "GET"
    }   
    response = lambda_handler.handler(event, None)
    body = response['body']
    assert body == "missing 'id' query String in request"

def test_lambda_handler_missing_method(mocker):
    event = {
        "queryStringParameters": {"id":"1"}
    }   
    response = lambda_handler.handler(event, None)
    body = response['body']
    assert body == "no method found"

def test_lambda_handler_invalid_method(mocker):
    event = {
        "httpMethod": "HEAD",
        "queryStringParameters": {"id":"1"}
    }
    response = lambda_handler.handler(event, None)
    body = response['body']
    assert body == "invalid method"

def test_lambda_handler_update(mocker):
    event = {
        "httpMethod": "POST",
        "queryStringParameters": {"id":"1"}
    }
    mocker.patch("User.User.getUserById", return_value=user)
    mocker.patch.object(User, 'update', return_value=200)
    response = lambda_handler.handler(event, None)
    body = response['body']
    assert body == "User with id#1 updated"

def return_status_200():
    return 200

def test_lambda_handler_update_not_found(mocker):
    event = {
        "httpMethod": "POST",
        "queryStringParameters": {"id":"1"}
    }
    mocker.patch("User.User.getUserById", return_value=None)
    response = lambda_handler.handler(event, None)
    body = response['body']
    assert body == "User with id#1 not found"

def test_lambda_handler_update_fail(mocker):
    event = {
        "httpMethod": "POST",
        "queryStringParameters": {"id":"1"}
    }
    mocker.patch("User.User.getUserById", return_value=user)
    mocker.patch.object(User, 'update', return_value=500)
    response = lambda_handler.handler(event, None)
    body = response['body']
    assert body == 'user update failed with status code 500'

def test_lambda_handler_delete(mocker):
    event = {
        "httpMethod": "DELETE",
        "queryStringParameters": {"id":"1"}
    }
    mocker.patch("User.User.getUserById", return_value=user)
    mocker.patch.object(User, 'delete', return_value=200)
    response = lambda_handler.handler(event, None)
    body = response['body']
    assert body == "User with id#1 deleted"

def test_lambda_handler_delete_not_found(mocker):
    event = {
        "httpMethod": "DELETE",
        "queryStringParameters": {"id":"1"}
    }
    mocker.patch("User.User.getUserById", return_value=None)
    response = lambda_handler.handler(event, None)
    body = response['body']
    assert body == "User with id#1 not found"

def test_lambda_handler_delete_fail(mocker):
    event = {
        "httpMethod": "DELETE",
        "queryStringParameters": {"id":"1"}
    }
    mocker.patch("User.User.getUserById", return_value=user)
    mocker.patch.object(User, 'delete', return_value=500)
    response = lambda_handler.handler(event, None)
    body = response['body']
    assert body == 'user delete failed with status code 500'