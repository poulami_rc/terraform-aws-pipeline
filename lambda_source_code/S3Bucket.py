import boto3
import json

class S3Bucket:
    bucketName = 'poulami-tf-assignment'
    client = boto3.client('s3')
    
    @staticmethod   
    def saveObj(obj, key):
        response = None
        try:
            S3Bucket.client.put_object(Body=obj, Bucket=S3Bucket.bucketName, Key=key)             
        except Exception as e:
            response = json.dumps(e.__dict__)
        return response
        
