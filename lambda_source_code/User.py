import boto3
import json

class User:
    table = boto3.resource('dynamodb').Table('tf-users')
    
    def __init__(self, UserId, UserName):
        self.userId = UserId
        self.userName = UserName
        
    def update(self, userName):
        response = User.table.update_item(
            Key={
                'UserId': self.userId
            },
            UpdateExpression="set UserName = :userName",
            ExpressionAttributeValues={
                ':userName': userName
            }
        )
        status_code = response['ResponseMetadata']['HTTPStatusCode']
        return status_code
    
    def delete(self):
        response = User.table.delete_item(
            Key={
                'UserId': self.userId
            }
        )
        status_code = response['ResponseMetadata']['HTTPStatusCode']
        return status_code
    
    @staticmethod   
    def getUserById(userId):
        response = User.table.get_item(Key={'UserId': userId})
        if 'Item' in response:
            return User(**response['Item'])
        return None