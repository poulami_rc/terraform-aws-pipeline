import json
from S3Bucket import S3Bucket
from User import User

def handler(event, context):
    userId = None
    if 'queryStringParameters' in event:
        queryStr = event['queryStringParameters']
        if 'id' in queryStr: 
            userId = queryStr['id']
    
    if userId is None:
        msg = "missing 'id' query String in request"
    else:
        if 'httpMethod' in event:
        
            if event['httpMethod'] == 'GET':
                user = User.getUserById(userId)
                if user is not None:
                    userjson = json.dumps(user.__dict__)
                    resp = S3Bucket.saveObj(userjson, 'userdata.json')
                    if resp is None:
                        msg = "User with id#"+userId+" found and saved to S3 bucket"
                    else:
                        msg = resp
                else:
                    msg = "User with id#"+userId+" not found"
                
            elif event['httpMethod'] == 'POST' or event['httpMethod'] == 'PUT':
                user = User.getUserById(userId)
                if user is not None:
                    status = user.update('Poulami')
                    if(status == 200):
                        msg = "User with id#"+userId+" updated"
                    else:
                        msg = 'user update failed with status code '+str(status)
                else:
                    msg = "User with id#"+userId+" not found"
        
            elif event['httpMethod'] == 'DELETE':
                user = User.getUserById(userId)
                if user is not None:
                    status = user.delete()
                    if(status == 200):
                        msg = "User with id#"+userId+" deleted"
                    else:
                        msg = 'user delete failed with status code '+str(status)
                else:
                    msg = "User with id#"+userId+" not found"
            else:
                msg = "invalid method"
        else:
            msg = "no method found"
    return {
        'statusCode': 200,
        'body': msg
    }