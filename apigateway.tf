# API Gateway
resource "aws_api_gateway_rest_api" "api" {
  name = "tf-api"
  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

#API Gateway -> resource
resource "aws_api_gateway_resource" "resource" {
  path_part   = "resource"
  parent_id   = aws_api_gateway_rest_api.api.root_resource_id
  rest_api_id = aws_api_gateway_rest_api.api.id
}

#API Gateway -> resource -> method
resource "aws_api_gateway_method" "method" {
  rest_api_id   = aws_api_gateway_rest_api.api.id
  resource_id   = aws_api_gateway_resource.resource.id
  http_method   = "ANY"
  authorization = "NONE"
}

#API Gateway -> resource -> method -> integration
resource "aws_api_gateway_integration" "integration" {
  rest_api_id             = aws_api_gateway_rest_api.api.id
  resource_id             = aws_api_gateway_resource.resource.id
  http_method             = aws_api_gateway_method.method.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.lambda.invoke_arn
}

# Lambda resource policy to allow invocation from ApiGateway
resource "aws_lambda_permission" "apigw_lambda" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn 	= "arn:aws:execute-api:${data.aws_region.current.name}:${local.account_id}:${aws_api_gateway_rest_api.api.id}/*/*${aws_api_gateway_resource.resource.path}"
}

# Apigateway deployment to stage
resource "aws_api_gateway_deployment" "deployment" {
  depends_on  = [aws_api_gateway_method.method, aws_api_gateway_integration.integration]
  rest_api_id = aws_api_gateway_rest_api.api.id  
  stage_name    = "test"
}