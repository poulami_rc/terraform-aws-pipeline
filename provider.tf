#configuration------------------------------------------------
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }

  backend "http" {
  }
}

provider "aws" {
  region = "ap-south-1"
}