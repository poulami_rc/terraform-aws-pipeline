### Implementation Details

The following list of AWS resources will be created in **ap-south-1** region:

- a DynamoDB table **"tf-users"** with UserId(String) as primary key. two items will be inserted at the time of table creation, each with two attributes - UserId (PK) and UserName
- A S3 bucket, named **"poulami-tf-assignment"**
- A custom IAM policy **"tf-s3-dynamodb-access-policy"** to allow read-write access to "tf-users" DynamoDB table and write access to "poulami-tf-assignment" s3 bucket
- A CloudWatch log group for lambda function
- A Lambda function **"tf-lambda"**:
    - the function will check for HTTP method in the request.
        - If HTTP method = "GET", it will retrive a specific item from DynamoDB table and save it in "poulami-tf-assignment" bucket with name "userdata.json". 
        - If HTTP method = "POST" or "PUT", it will update the "UserName" attribute of a specific DynamoDB table item
        - If HTTP method = "DELETE", it will delete a specific item from DynamoDB table
    - the following three Python files will be uploaded as lambda source code:
        - lambda.py: contains the handler and business logic
        - User.py: Object class corresponding to DynamoDB table
        - S3Bucket.py: Object class corresponding to S3 bucket
    - the following IAM policies will associated to lambda IAM role
        - AWSLambdaBasicExecutionRole (AWS managed): will allow Lambda function to put logs into the CloudWatch log created
        - tf-s3-dynamodb-access-policy (Custom): will allow access to DynamoDB table and S3 bucket 
- a APIGateway, **"tf-api"** with a single resource **"/resource"** and a single proxy method (ANY) integrated with "tf-lambda". The api will be deployed with stage **"test"**

### Scope of Improvement

- Currently AWS resource names along with region are hard-coded, to increase reusability those can be parameterised with introduction of variables.
