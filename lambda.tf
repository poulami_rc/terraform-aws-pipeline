#CloudWatch log for lambda
resource "aws_cloudwatch_log_group" "lambda_log_group" {
  name              = "/aws/lambda/tf_lambda"
  retention_in_days = 7
  lifecycle {
    prevent_destroy = false
  }
}

#Lambda function code
data "archive_file" "lambda_zip" {
    type          = "zip"
    source_dir    = "${path.module}/lambda_source_code"
    output_path   = "${path.module}/lambda_function.zip"
    depends_on = [random_string.r]
}

resource "random_string" "r" {
  length  = 16
  special = false
}

#Lambda function configuration
resource "aws_lambda_function" "lambda" {
  filename         = data.archive_file.lambda_zip.output_path
  function_name    = "tf_lambda"
  role             = "${aws_iam_role.iam_for_lambda_tf.arn}"
  handler          = "lambda_handler.handler"
  source_code_hash = "${data.archive_file.lambda_zip.output_base64sha256}"
  runtime          = "python3.10"
  timeout          = 10
  depends_on       = [aws_cloudwatch_log_group.lambda_log_group]
}