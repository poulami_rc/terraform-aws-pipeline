#dynamodb table
resource "aws_dynamodb_table" "dynamodb" {
  name           = "tf-users"
  billing_mode   = "PROVISIONED"
  read_capacity  = 1
  write_capacity = 1
  hash_key       = "UserId"
  
  attribute {
    name = "UserId"
    type = "S"
  }
}

#insert record in dynamodb table
resource "aws_dynamodb_table_item" "item1" {
  table_name = aws_dynamodb_table.dynamodb.name
  hash_key   = aws_dynamodb_table.dynamodb.hash_key

  item = <<ITEM
{
  "UserId": {"S": "1"},
  "UserName": {"S": "John Doe"}
}
ITEM
}

resource "aws_dynamodb_table_item" "item2" {
  table_name = aws_dynamodb_table.dynamodb.name
  hash_key   = aws_dynamodb_table.dynamodb.hash_key

  item = <<ITEM
{
  "UserId": {"S": "2"},
  "UserName": {"S": "Jane Doe"}
}
ITEM
}