#policy document to grant read-write access to dynamodb table & write access to s3 bucket
data "aws_iam_policy_document" "s3-dynamodb-access" {
  statement {
    sid = "1"

    actions = [
      "s3:PutObject",
      "dynamodb:PutItem",
      "dynamodb:DeleteItem",
      "dynamodb:GetItem",
      "dynamodb:Scan",
      "dynamodb:Query",
      "dynamodb:UpdateItem",
      "dynamodb:GetRecords"
    ]

    resources = [
      "${aws_dynamodb_table.dynamodb.arn}",
      "${aws_s3_bucket.s3-bucket.arn}/*"
    ]
  } 
}

#custom policy with the policy document
resource "aws_iam_policy" "s3-dynamodb-access" {
  name   = "tf-s3-dynamodb-access-policy"
  path   = "/"
  policy = data.aws_iam_policy_document.s3-dynamodb-access.json
}

#lambda iam role
resource "aws_iam_role" "iam_for_lambda_tf" {
  name = "iam_for_lambda_tf"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

#use AWS-managed AWSLambdaBasicExecutionRole 
data "aws_iam_policy" "lambda_basic_execution_policy" {
  arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

#attach AWS-managed AWSLambdaBasicExecutionRole policy to Lambda IAM role
resource "aws_iam_role_policy_attachment" "lambda_flow_log_cloudwatch" {
  role       = aws_iam_role.iam_for_lambda_tf.id
  policy_arn = data.aws_iam_policy.lambda_basic_execution_policy.arn
}

#attach custom access policy to Lambda IAM role
resource "aws_iam_role_policy_attachment" "lambda_s3-dynamodb-access" {
  role       = aws_iam_role.iam_for_lambda_tf.id
  policy_arn = aws_iam_policy.s3-dynamodb-access.arn
}